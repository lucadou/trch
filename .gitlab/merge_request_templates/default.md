# Version: [version number]

## Notable Changes:

* Item1
* Item2
* Item3

### Explanations of Changes (if Needed):



## Related Issues and Merge Requests

*(You can delete any lines that do not apply.
  If no lines apply, you can delete this section.)*

* This MR closes #X, #Y, #Z
* This MR is part of #X, #Y, #Z
* This MR is related to !X, !Y, !Z

## Merge Checklist:

- [ ] Changelog entry
- [ ] Changes tested (if needed)
- [ ] Documentation updated (if needed)
- [ ] Readme updated (if needed)
- [ ] Translations file header has version bumped
- [ ] Translations file application footer has version bumped
- [ ] Translations have been recompiled
- [ ] Unnecessary git tags for previous commits in this MR removed
      (`git tag -d vX.Y.Z`, `git push --delete origin vX.Y.Z`)
- [ ] Git tag created for final commit prior to merge (`git tag vX.Y.Z`)
- [ ] Git tag pushed (`git push origin vX.Y.Z`)
- [ ] CI pipeline passed on final commit prior to merge
