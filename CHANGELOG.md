# Changelog

All notable changes to this project will be documented in this file.

Each release will require bumping the version in the application footer
and translation file header, along with recompiling the translations.

## [2.1.3] - 2024-05-06

### Added

- Note about Google Cloud Run caching old versions of the container

### Changed

- Adjusted link to Compact YouTube to use new domain

## [2.1.2] - 2024-05-04

### Added

- Another screenshot to the documentation images
- Before/after screenshots to the homepage (#20)
- JPEG-XL versions of documentation images

### Changed

- Line about hiding from "Recommended Channels" section to note that it also
  hides from the "<current channel> Viewers Also Watch" and "Following" sections.
- Recompiled translations for version stamp and before/after screenshot alt-text

## [2.1.1] - 2024-04-15

### Added

- Integration tests (#9)

### Changed

- Fixed incorrect mentions of Compact YouTube in readme (#19)
- Rename issue template to be default template
- Wording clarification of checklist item in MR template

## [2.1.0] - 2024-04-11

### Added

- Additional permission for GCP TRCH deployer (#12)
- Document response times (#14)
- Exact running costs, and a determination of if Origin Shield is worth it (#13)
- JPEG-XL instructional images (#16)
- Language header to CloudFront cache (#18)
- Link to Compact YouTube (#17)
- Min/max Cloud Run instance count documentation (#15)
- Min/max Cloud Run instance count variables (#15)
- Merge request/Issue templates (#8)
- Nginx/systemd/venv deployment instructions (#4)

## [2.0.0] - 2024-04-09

### Added

- Cache-control headers (#2)
- Favicon
- GCR container build CI stage
- OpenTofu deployment instructions
- OpenTofu files
- OpenTofu CI stage

Changed

- Redid CONTRIBUTING.md
- Redid screenshots for instructions (#11).

Removed

- Todo list from README.md (migrated to Issues)

## [1.3.0] - 2024-03-04

### Added

- Form validation prior to displaying modal

### Changed

- Moved dev sections of readme to Contributing document

### Removed

- Pipenv support

## [1.2.0] - 2024-03-03

### Added

- Dockerfile
- Docker instructions
- Google Cloud Run instructions
- Nginx conf files
- Nginx setup instructions
- Readme table of contents
- Venv support

## [1.1.0] - 2024-03-02

### Added

- Gitignore
- Readme (fleshed out)
- Unit tests and code coverage

## [1.0.0] - 2024-03-02

### Added

- Changelog
- License
- MVP application
