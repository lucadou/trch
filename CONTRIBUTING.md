# Contributing

Twitch Recommended Channel Hider welcomes your contributions.

If you would like to suggest features, report bugs, etc., feel free to open an
[Issue](https://gitlab.com/lucadou/trch/-/issues).

If you would like to help develop the application, merge requests are welcome.
For major changes, please open an issue first to discuss what you would like
to change.

### Table of Contents

* [Contributor Code of Conduct](#contributor-code-of-conduct)
* [Issue-Writing Guidelines](#issue-writing-guidelines)
* [Environment Setup](#environment-setup)
  * [Re-Compiling/Updating Translations](#re-compilingupdating-translations)
* [Running](#running-the-application)
  * [With Docker](#with-docker)
  * [With venv](#with-venv)
* [Local Deployment](#local-deployment)
  * [With Docker and Nginx](#with-docker-and-nginx)
  * [With Nginx, systemd, and venv](#with-nginx-systemd-and-venv)
* [Cloud Deployment](#cloud-deployment)
  * [Google Cloud Run](#google-cloud-run)
  * [OpenTofu (Terraform)](#opentofu-terraform)
    * [OpenTofu Prerequisites](#opentofu-prerequisites)
    * [OpenTofu Deployment](#opentofu-deployment)
* [Testing](#testing)
* [Localization](#localization)
* [Forking Considerations](#forking-considerations)
  * [GCP Service Account](#gcp-service-account)
  * [AWS Service Account](#aws-service-account)
  * [OpenTofu Backend Configuration File](#opentofu-backend-configuration-file)
* [Coding Style](#coding-style)
  * [JavaScript](#javascript)
  * [Markdown](#markdown)
  * [Python](#python)
* [Notes/FAQ](#notesfaq)

## Contributor Code of Conduct

If you are going to contribute to this project in any way (bug reports, merge
requests, etc.), please follow these guidelines:

* Keep things civil and respectful.
* Do not harass contributors, on- or off-site.

This project is purely a volunteer effort written in our spare time,
so please be courteous.

## Issue-Writing Guidelines

Please follow these guidelines when reporting issues:

* Give your issue a clear, descriptive title.
* Be accurate and precise.
* If you have multiple problems, create a separate issue for each one.
* When available and applicable, use issue templates.

Reported issues not following these guidelines may be closed without warning.

## Environment Setup

If you wish to contribute to this project, you will need the following:

* Node LTS/Gallium.
  * Not sure what the minimum Node version is, Gallium was just the version
    used to develop this.
  * Only required for IDE hints.
  * Set up your development environment with the following commands:
    ```bash
    npm ci
    ```
* Python 3.9+.
  * Application was developed with 3.10 but 3.9 is the minimum required
    for some [Typing extensions](https://stackoverflow.com/a/39458225/15264046).
  * Formatting/linting rules are checked with flake8 and pylint (via Pipenv)
  * The venv package.
    * For Debian and Debian-based distro users,
      you will need to install your distro's `python3.x-venv` package.
    * Outside the Debian world, it should be part of the stdlib.
  * Set up your development environment with the following commands:
    ```bash
    python3 -m venv venv
    source venv/bin/activate
    pip install -r requirements/dev.txt  
    ```
  * When developing, it is recommended that you use
    [blackd](https://black.readthedocs.io/en/stable/usage_and_configuration/black_as_a_server.html)
    and an IDE plugin such as
    [BlackConnect](https://plugins.jetbrains.com/plugin/14321-blackconnect)
    to auto-format as you go.
* Docker (optional).
  * Only needed if you intend on running this locally via Docker.
* OpenTofu, AWS, and GCP (optional).
  * Only needed if you intend on deploying to the cloud and
    running/debugging the process locally.

### Re-Compiling/Updating Translations

See: [Localization](#localization).

## Running the Application

### With Docker

```bash
docker build -t trch .
docker run -p 5000:8080 trch:latest  # Do not modify second port (8080); it needs to match the port in the Dockerfile.
```

### With venv

```bash
python -m venv venv
source venv/bin/activate
flask --app app/trch run  # Will start on port 5000 by default; set $PORT to override.
# Alternatively:
source venv/bin/activate
cd app/
gunicorn trch:app
```

## Local Deployment

### With Nginx, systemd, and venv

```bash
# After downloading TRCH
python -m venv venv
source venv/bin/activate
pip install -r requirements/prod.txt
# Modify the following files accordingly:
# - docs/nginx-service-venv.conf
# - docs/systemd-service-venv.service
# - docs/systemd-socket-venv.socket
# Then:
SERVICE_NAME="trch"
cp "docs/nginx-service-venv.conf" "/etc/nginx/conf.d/${SERVICE_NAME}.conf"
cp "docs/systemd-service-venv.service" "/etc/systemd/${SERVICE_NAME}.service"
cp "docs/systemd-socket-venv.socket" "/etc/systemd/${SERVICE_NAME}.socket"
nginx -t
# When nginx -t gives a clean bill of health:
systemctl daemon-reload
systemctl reload nginx.service
systemctl enable --now ${SERVICE_NAME}.socket
systemctl enable --now ${SERVICE_NAME}.service
```

### With Docker and Nginx

```bash
docker run -d --restart unless-stopped -p 5000:8080 trch:latest
# Modify docs/nginx-service-docker.conf accordingly, then:
SERVICE_NAME="trch"
cp "docs/nginx-service-docker.conf" "/etc/nginx/conf.d/${SERVICE_NAME}.conf"
nginx -t
# When nginx -t gives a clean bill of health:
systemctl reload nginx.service
```

## Cloud Deployment

### Google Cloud Run

While the OpenTofu (Terraform) method also uses Google Cloud Run,
this deploys straight to Google Cloud Run - no CDN or anything extra.

This is good for quick-and-dirty deployments to quickly test changes.

```bash
gcloud auth login
gcloud run deploy
# Leave the source code location at the default value.
# Set the service name or leave it at the default.
# If prompted to enable any GCP APIs, say yes.
# If prompted, pick whichever region you like.
# When prompted if you want to enable unauthenticated access,
#  because it is not being fronted by a CDN,
#  you MUST say yes or the Cloud Run instance will be inaccessible.
gcloud run deploy trch-service --source .
```

### OpenTofu (Terraform)

#### OpenTofu Prerequisites

OpenTofu:

```bash
cd tofu/
cp backend.conf.example backend.conf
cp tfvars.sh.example tfvars.sh
# Edit backend.conf and tfvars.sh, then...
source tfvars.sh
tofu init -backend-config=backend.conf
```

Google Cloud Artifact Registry repository:

```bash
cd tofu/
source tfvars.sh
cd ..
gcloud artifacts repositories create ${TF_VAR_gcp_ar_repo_name} --repository-format=docker \
    --location=${TF_VAR_gcp_region} --description="TRCH Docker repository"
```

Build image:

```bash
cd tofu/
source tfvars.sh
cd ..
gcloud builds submit --tag ${TF_VAR_gcp_region}-docker.pkg.dev/${TF_VAR_gcp_project}/${TF_VAR_gcp_ar_repo_name}/${TF_VAR_gcp_ar_image_name}
```

#### OpenTofu Deployment

```bash
cd tofu/
source tfvars.sh
tofu apply
```

NOTE: After deployment, you *must* go into the Cloud Run GUI > Cloud Run service > Security
and set "Authentication" to "Allow unauthenticated invocations",
as Terraform does not provide a way to do this automatically.

## Testing

This application uses pytest as its test runner, both locally and in CI.

```bash
source venv/bin/activate
coverage run -m pytest integration_tests/
coverage html --fail-under=90 -d ./cover
```

## Performance

See [PERFORMANCE](PERFORMANCE.md) for more information
on how fast the application is expected to respond to requests
and what you can do to speed it up further when deployed to the cloud.

## Localization

To create translations, please follow these steps (taken from
[these docs](https://flask-babel.tkte.ch/#translating-applications)):

1. Inside the [application file](app/trch.py), add the
   [ISO 639-1 code](https://en.wikipedia.org/wiki/List_of_ISO_639-1_codes)
   and name to the `app.config['LANGUAGES']` dict.
2. Execute the following commands:
   ```bash
   source venv/bin/activate
   cd app/
   # Set language
   export ISO639_LANGUAGE="<insert language code here>"
   # Extract strings
   pybabel extract -F babel.cfg -o messages.pot --input-dirs=.
   # Create PO file (the file with the strings to translate)
   pybabel init -i messages.pot -d translations -l "${ISO639_LANGUAGE}"
   ```
3. Edit the generated PO file (take a look at the English file for the comment
   block and translation info string), then run this command:
   ```bash
   pybabel compile -d translations
   ```
4. Test the translations by starting up the application and using it.

## Forking Considerations

Want to develop your own branch with CI before submitting a merge request,
or maintain a hard-fork of the project?

When forking, you will need to maintain your own CI configuration,
as you will have to run pipelines under your account.
Specifically, the following variables must be populated:

* All the variables listed in [tfvars](tofu/tfvars.sh.example):
  * `TF_VAR_aws_route53_zone_id` - The Route53 Zone ID.
  * `TF_VAR_domain` - The FQDN the application will be available at via the CDN.
  * `TF_VAR_gcp_project` - The GCP project ID, including the random characters at the end.
  * `TF_VAR_gcp_region` - The GCP region to deploy to.
  * `TF_VAR_gcp_ar_image_name` - The image name in the Artifact Registry.
  * `TF_VAR_gcp_ar_repo_name` - The Artifact Registry repository name.
* And the following:
  * `GCP_TF_SA` - The GCP principal (service account name/ID) OpenTofu will authenticate with.
  * `GCP_TF_SA_KEY` - The GCP service account key file OpenTofu will authenticate with (see below).
    * This must be saved with the type [File](https://docs.gitlab.com/ee/ci/variables/index.html#use-file-type-cicd-variables).
  * `AWS_TF_SA_KEY` - The AWS service account key file OpenTofu will authenticate with (see below).
    * This must be saved with the type [File](https://docs.gitlab.com/ee/ci/variables/index.html#use-file-type-cicd-variables).
  * `TF_CONF_FILE` - The OpenTofu backend.conf file (see below).
    * This must be saved with the type [File](https://docs.gitlab.com/ee/ci/variables/index.html#use-file-type-cicd-variables).

### GCP Service Account

For the GCP service account, you should create a custom GCP IAM role
with permissions from these roles and permissions:

* Cloud Build Service Account (roles/cloudbuild.builds.builder).
* Cloud Run Developer (roles/run.developer).
* Service Account Admin (roles/iam.serviceAccountAdmin).
* Permission run.services.setIamPolicy.
  * This lets OpenTofu manage the service accounts used to access the Cloud Run service.
* Viewer (roles/viewer).
  * For streaming logs from Cloud Build.
  * Why project viewer is needed to view this is unclear; you just do.

Then create a new service account using this custom role.

After creating your account, you will need to get a key from the account to load in to GitLab CI:

```bash
export GCP_SVC_ACCT="my-service-account"
export GCP_PROJECT="my-gcp-project-id"
gcloud iam service-accounts keys create ~/.config/gcloud/sa-trch-deployer.json --iam-account=${GCP_SVC_ACCT}@${GCP_PROJECT}.iam.gserviceaccount.com
cat ~/.config/gcloud/sa-trch-deployer.json | base64 --wrap=0
```

Copy the base64'd key file into GitLab.

### AWS Service Account

For the AWS service account, you should create a custom IAM user
belonging to a role or group with the following policies attached:

* AmazonRoute53FullAccess
* AWSCertificateManagerFullAccess
* CloudFrontFullAccess

This will give you more permissions than required,
but if you want a quick-and-dirty solution,
this works just fine.

Alternatively, as importable JSON:

```json
{
    "AttachedPolicies": [
        {
            "PolicyName": "AmazonRoute53FullAccess",
            "PolicyArn": "arn:aws:iam::aws:policy/AmazonRoute53FullAccess"
        },
        {
            "PolicyName": "CloudFrontFullAccess",
            "PolicyArn": "arn:aws:iam::aws:policy/CloudFrontFullAccess"
        },
        {
            "PolicyName": "AWSCertificateManagerFullAccess",
            "PolicyArn": "arn:aws:iam::aws:policy/AWSCertificateManagerFullAccess"
        }
    ]
}
```

After you have created the user, in the IAM console:

* Open the user.
* Go to the "Security credentials" tab.
* Scroll down to "Access keys" and click the "Create access key" button.
* Under "Use case", select "Command Line Interface (CLI)".
* Click the "Next" button.
* Add a descriptive name for it, then click "Create access key".
* Click the "Download .csv file" button.

After downloading the credential CSV, you will need to base64-encode it.

```bash
cat aws-access-key-file.csv | base64 --wrap=0
```

Copy the base64'd key file into GitLab.

### OpenTofu Backend Configuration File

After filling out the OpenTofu `backend.conf` file in
[OpenTofu (Terraform)](#opentofu-terraform), you will need to base64-encode it:

```bash
cat tofu/backend.conf | base64 --wrap=0
```

Copy the base64'd backend file into GitLab.

## Coding Style

### JavaScript

* Variables must be declared using `let` or `const`.
  * No `var`, as it is scoped too broadly.
  * No implicit declarations, to avoid
    [The Horror of Implicit Globals](http://blog.niftysnippets.org/2008/03/horror-of-implicit-globals.html).
* Classes and methods must use [JSDoc docstrings](https://jsdoc.app/).
* Source files must have a copyright and GPL header at the top.
  * JS files must also have
    [LibreJS tags](https://www.gnu.org/software/librejs/free-your-javascript.html#magnet-link-license).
* Variable names must be descriptive and use lowerCamelCase.
* Follow the
  [MDN styling guidelines](https://developer.mozilla.org/en-US/docs/MDN/Writing_guidelines/Writing_style_guide/Code_style_guide/JavaScript).

### Markdown

* For those in smaller terminals (and to keep git diffs sensible),
  try and keep lines 80 characters or less.
  * We recommend breaking long sentences onto multiple lines,
    and each sentence onto its own line, to help maintain this.
  * Exceptions are allowed for things like links.

### Python

* Code must pass black, flake8, and pylint with no errors or warnings.
  * You can check your code using the following steps:
    ```bash
    source venv/bin/activate
    black --check .
    flake8 .
    pylint .
    ```
  * The following exceptions are allowed:
    * Conflicts between the tools - black generally has the best formatting,
      so when conflicts arise (e.g. line length),
      adjust them to favor black's style unless there is a pressing reason not to.
    * Unavoidably long lines (e.g. links in comments).
      * To get flake8 to ignore it, append this to the end of the line:
        ```python
        # noqa: E501
        ```
    * Import errors caused by importing other classes in the project
      (and which work just fine when the program is run).
      * To get pylint to ignore it, append this to the end of the line:
        ```python
        # pylint: disable=import-error
        ```
* *Always* use [type hints](https://docs.python.org/3/library/typing.html),
  and make them as detailed as you can.
  * This makes IDE type checking more accurate and makes the code more
    maintainable.
  * When using dicts with non-trivial structures or importing
    configuration files (e.g. JSON), TypedDicts are mandatory.
  * The only time it is acceptable to not use detailed type hints is if you are
    interacting with a library that constructs specific types on the fly
    (e.g. boto3 for AWS has some classes only available at runtime which cause
    errors when trying to create type hints for them).
* Imports should not be compacted (i.e. no `from X import Y` or `import X.Y`).
  * There are several exceptions to this rule:
    * Type hints (e.g. `from typing import Tuple` is allowed).
    * Importing another file from within the project hierarchy.
      * This is because relative imports in Python can be a nightmare - sometimes
        they work perfectly and `from X import Y` is broken, other times it's
        the reverse.
    * Classes which misbehave if you do not do `from X import Y`
      (e.g. some Flask and Flask-Babel classe).
    * Classes which are not being used for typing.
  * The reason for this is because, when using type hints, using compacted
    imports can confuse IDEs, especially for more detailed type hints.
    * This does mean the code is less compact (less "Pythonic", even), but the
      benefits outweigh the potential annoyance of longer lines.
  * Example:
    ```python
    # Do not do this
    from pathlib import Path
    bad_path: Path = Path('...')
    # Instead, do this
    import pathlib
    good_path: pathlib.Path = pathlib.Path('...')
    ```
* Do not use magic numbers/values - instead, unless it's for logging, it should
  be defined in a constants file.
* Log liberally.
* Use f-strings when you need to do string interpolation.
  * Exception: logging statements should use `%s`:
    * https://stackoverflow.com/a/54368109
    * https://docs.python.org/3/howto/logging.html#optimization
  * Example:
    ```python
    i: int = 123
    print(f'{i} * 2 = {i * 2}') # => '123 * 2 = 246'
    logger.info('%i * 2 = %i', i, (i * 2)) # => '123 * 2 = 246'
    ```
* Classes and methods must use
  [reStructuredText docstrings](http://daouzli.com/blog/docstring.html#restructuredtext).
  * All classes and methods must provide a detailed description of what they
    do, along with any input value restrictions, implicit assumptions, and
    error cases.
  * If a return type is non-trivial, specific examples must be given showing
    the results of various input combinations.
  * Exceptions that may be raised/passed back to the caller must be detailed
    using this syntax:
    ```python
    :raise ExceptionType: exception description and how it is triggered
    ```
  * See also: [reST Sphinx documentation](https://www.sphinx-doc.org/en/master/usage/restructuredtext/index.html).
* Source files must have a copyright and GPL header at the top.
* Variable names must be descriptive and use snake_case.

## Notes/FAQ

> When attempting to run TRCH locally, I get the following error:
>
>  ```bash
>  $ flask run --app app/trch
>  Usage: flask run [OPTIONS]
>  Try 'flask run --help' for help.
>  
>  Error: No such option: --app
>  ```

You must put `run` *after* the `--app` parameter,
i.e. `flask --app app/trch run`.

> Why run in Cloud Run and front it with CloudFront? Why not go all-in on AWS or GCP?

There is no technical reason;
Cloud Run is just easier to get running with Flask apps than Google Cloud Functions and AWS Lambda.

In the future, the use of Google Cloud Build will likely be discontinued (#5),
alongside Google Cloud Run (#6, assuming AWS has on-demand equivalents),
but there is a very good reason why we have not gone all-in on GCP -
their CDN offerings:

* [Apigee](https://cloud.google.com/apigee/pricing?hl=en)
  * Minimum monthly spend: $20
* [Cloud CDN](https://cloud.google.com/cdn?hl=en#pricing),
  * Egress, cache misses: $0.08/GB for the first 10 TB
  * Egress, cache hits: $0.01/GB to NA/Europe
  * Cache lookups: $0.0075/10k HTTP/S requests
  * This seems reasonable, but using Cloud CDN also requires using an
    [Internal Application Load Balancer](https://cloud.google.com/vpc/network-pricing#internal-https-lb)
    * $0.0327/hr per proxy instance, minimum 1 instance, even when not in use
      * That's $23.544/30 days

CloudFront, by comparison, has no monthly minimum or mandatory load balancers -
it is purely pay-as-you-go.
Even with the triple-egress fees
(Cloud Run -> internet, internet origin -> CloudFront, CloudFront -> requestor),
it comes out well ahead of Google Cloud CDN (at current usage).

The discontinuation of Cloud Build and Cloud Run are not high priority
(at least, not until the monthly bills make Cloud CDN look reasonable),
so for the time being, we are split between the two cloud providers.

AWS CloudFront was chosen over CloudFlare because it's good enough
and is easier to get running.

> When attempting to deploy TRCH in AWS, Terraform is throwing permissions errors. What should I do?

Ensure your AWS service account is set up as described in
[AWS Service Account](#aws-service-account).

> How much does TRCH cost to run in GCP and AWS?

TODO (#13)

* AWS CloudFront https://aws.amazon.com/cloudfront/pricing/
  * Base CF
    * $0.020/GB data transfer to origin
    * $0.085/GB data transfer to users (to internet technically, but since we are hosting the app in GCP, it kinda is already internet...whatever) for first 10 TB
    * $0.010-0.012/10k HTTPS requests for default price class (100); up to $0.010-0.220 otherwise
    * Since this is a single-region app by default, I'm not sure if there is any real benefit to using anything but price class 100
  * Origin Shield caching layer - see Additional Features tab
    * Also see: https://docs.aws.amazon.com/AmazonCloudFront/latest/DeveloperGuide/origin-shield.html
    * $0.0075-0.0090/10k HTTP requests for default price class (100); up to $0.0075-0.0160 otherwise
    * Could save money on data transfer to origin and could save on Cloud Run costs
    * TODO (#13) run the numbers and figure out under what circumstances this would save money
      * If it is unlikely to save me money, disable it by default
* AWS Certificate Manager https://aws.amazon.com/certificate-manager/pricing/
  * Free for public certificates
* GCP Cloud Build https://cloud.google.com/build#pricing
  * First 2,500 build minutes per month - free
  * $0.006/min after
  * Average time per build - 30 seconds - 2 minutes (I have yet to see it take >2 min; usually <1)
  * Average price per month - free
* GCP Artifact Registry https://cloud.google.com/artifact-registry/pricing
  * Up to 512 MB - Free
  * &gt;512 MB - $0.10/GB
  * Image takes up ~400 MB
  * Average price per month - if you purge old builds, free; otherwise, $0.10 per 2 images
* GCP Cloud Run https://cloud.google.com/run/pricing
  * TODO (#13)

> After deploying to AWS and GCP, when attempting to access it via the CloudFront distribution,
> I get error 403 "Your client does not have permission to get URL / from this server."
> Why is this?

After deployment, you must manually go into
the GCP console > Cloud Run > (the Cloud Run service) > Security
and set "Authentication" to "Allow unauthenticated invocations",
as Terraform does not provide a way to do this automatically.

This step will likely be obsoleted eventually as part of #1,
which will authenticate CloudFront to Cloud Run.

> After deploying a new version that updated the filter string,
> it is still serving the old version. Why is this?

That would be because CloudFront has a stale version in its cache.
To remedy this:

1. Open the CloudFront console.
2. Open the distribution.
3. Go to the "Invalidations" tab.
4. Click on the "Create invalidation" button.
5. In the object paths textbox, add the following:
   ```
   /lists/*
   ```
6. Click on the "Create invalidation" button.
7. Wait until the "Status" is "Completed".
8. The stale cache should be cleared and the updated version should now be served.

> I am attempting to set up my own fork, but am getting CI errors when
> attempting to authenticate to AWS:  
> ```
> Expected header "User Name" not found
> ```  
> Why is this?

According to [this GitHub issue](https://github.com/aws/aws-cli/issues/7721), there are two possibilities:

* The CSV is missing the "User name" column.
  * You can determine this through the output of `head -n 1 aws-access-key-file.csv`,
    which should match this:
  * `User name,Access key ID,Secret access key`
  * On the second line, this should be set to the
    user name of the user you created in the IAM console.
  * I have observed this happen with a freshly-downloaded key file from the IAM console,
    so there appears to be a bug on AWS' end that can cause this.
* The file is encoded incorrectly.
  * The IAM console produces UTF-8 with BOM by default,
    but the AWS CLI requires plain UTF-8.
  * You can determine this through the output of `file aws-access-key-file.csv`,
    which should match one of these:
    * `ASCII text`
    * `Unicode text, UTF-8 text`
  * Re-save in your spreadsheet editor of choice,
    then re-encode as base64 and re-upload the value to the GitLab CI variables page.

> Why do we have to create the Google Cloud Artifact Registry repository manually?
> Why not use OpenTofu's [google\_artifact\_registry\_repository](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/artifact_registry_repository) resource?

This is mainly for ease of initial deployment.

While Terraform is quite nice, it is very easy to end up in a chicken-and-egg situation:

* You cannot deploy the Google Cloud Run service without a successful build,
* But you cannot perform a Cloud Build without an Artifact Registry repository.

Now, if this were a closed-source corporate application,
there would be a very obvious and reasonable path forward here:

* Create the Artifact Registry repository resource first.
* Add the Google Cloud Run resource in after completing your first build.

However, this poses a serious challenge for anyone else who comes along
and wants to deploy the application for themselves, unless they know about
[resource targeting](https://developer.hashicorp.com/terraform/tutorials/state/resource-targeting).

If we incorporate the Artifact Registry repository as an OpenTofu resource,
we must provide extra instructions for a manual first-time deployment utilizing
resource targeting, which can already be intimidating even for those who use
it professionally.

This would also preclude users from being able to do a first-time deployment via CI,
since you cannot get to the deployment stage without first passing the build stage.

With all this in mind, the decision has been made to require manual management
of Artifact Registry repositories.

In addition, Artifact Registry will be phased out in the future (#5),
so it makes little sense to build up extra tooling and guidance around it
when its days are numbered.

Finally, we are not tracking the Artifact Registry via a data block, either,
because Artifact Registry repositories do not have any useful
exported attributes for constructing image paths.

> I am trying to deploy a new version,
> but even when I access the Cloud Run URL directly (bypassing the CDN cache),
> I still get an old version of TRCH. What's going on?

Even though the Cloud Run configuration is set to use the latest version
of the container from the Artifact Repository,
it can hold on to the latest version that it was deployed with.

Even if you delete the underlying container from Artifact Registry,
Cloud Run may continue to run it.

When this happens:

1. Open the service in the Cloud Run console
2. Go to "Edit & Deploy New Revision"
3. Press "Deploy"

This will dump the image Cloud Run has cached
in favor of the current latest image every time.

One has to wonder how long the underlying container has to be deleted before
the Cloud Run cache gets evicted on its own
(and if this behavior could be taken advantage of
to avoid paying for container storage entirely).
