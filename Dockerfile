FROM python:3

# Have Python statements/logs immediately appear in Cloud Run logs
ENV PYTHONUNBUFFERED True

WORKDIR /
# Install dependencies
COPY requirements.txt ./
RUN mkdir requirements
COPY requirements ./requirements/
RUN pip install -r requirements.txt

# Copy files
COPY app ./
# Build translations
RUN pybabel compile -d translations

ENV PORT 8080
CMD gunicorn --bind :$PORT --workers 4 --threads 8 --log-level 'debug' trch:app
