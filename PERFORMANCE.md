# Performance

All prices in this document were computed on 2024-04-12;
they will likely increase with time.

If you are interested in deploying your own TRCH instance to the cloud,
you may wonder: "how performant is it?"
Especially since the Google Cloud Run service is not kept alive 24/7 by default
(because that would cost more money),
you may wonder how fast Cloud Run can start.

### Table of Contents

* [Introduction](#introduction)
* [Benchmarks](#benchmarks)
* [Performance Tuning](#performance-tuning)
  * [Variables](#variables)
* [Closing Considerations](#closing-considerations)

## Introduction

For those not familiar with Google Cloud Run,
here is what you need to know for the purposes of these benchmarks:

* When launched per-request (as opposed to always-allocated),
  Cloud Run keeps the instance running for 10-15 minutes after the last request.
* If no new requests come in during that period, the instance is shut down.
* When a new request comes in, if no instances are already running
  (or those running are "overloaded"), it will spin up a new instance.
  * Per [this Stack Overflow answer](https://stackoverflow.com/a/75542509/15264046),
    Cloud Run will spin up another instance when all available instances
    exceed 60% CPU utilization.
  * Currently, the minimum number of instances is 0, and the maximum is 2.
  * To keep the function always hot,
    increase the minimum instance count to at least 1.
  * To increase the maximum capacity of the application,
    increase the maximum instance count.
    * For the potential cost impacts of this, see [Performance Tuning](#performance-tuning).

## Benchmarks

For the homepage, you can expect performance as follows:

| Homepage   | Cold Instance              | Warm Instance             |
|------------|----------------------------|---------------------------|
| Cache Hit  | 0.10/29.09/1.92/31.11      | 0.12/45.18/1.66/46.91     |
| Cache Miss | 0.094/2120/1.69/2121.784   | 0.097/117.11/1.56/118.767 |

For lists, you can expect performance as follows:

| List       | Cold Instance              | Warm Instance             |
|------------|----------------------------|---------------------------|
| Cache Hit  | 0.095/37.05/1.47/38.615    | 0.14/32.21/1.45/33.8      |
| Cache Miss | 0.095/2390/1.76/2391.855   | 0.10/105.87/1.97/107.94   |

Explanation:

* Cache hit - As the name implies,
  CloudFront successfully pulled a response from its cache.
* Cache miss - As the name implies,
  CloudFront could not pull a response from its cache
  and had to reach out to the service.
* Cold instance - An instance of the Google Cloud Run service was not already running
  and had to undergo a cold startup before accepting the request.
* Warm instance - An instance of the Google Cloud Run service was already running
  and was able to accept the request immediately. 
* Each cell is in the format
  `time to connect/time spent waiting for server response/time spent downloading response/total`
  and is in milliseconds.

These were taken from a wired dual-stack internet connection in North Carolina,
going to the Northern Virginia datacenters of AWS and GCP.
The connection has an average ping time to the CloudFront distribution of 30 ms.

The sample size for each category is n=1,
so these benchmarks are far from scientific,
but they should give you an idea as to the performance of the application.

## Performance Tuning

As you can see, a non-trivial amount of time is spent waiting for Cloud Run
to start the container,
though the time to first contentful paint
(and it's a full paint, rather than just a splash screen)
still pales in comparison to some websites maintained by large companies.

If you would like to decrease the time it takes the application to load
(i.e. eliminate the cold boot time),
override the `min_instance_count` OpenTofu variables
(see [Variables](#variables) for more information)
in your `tfvars.sh` file and CI configuration (if applicable).

By keeping at least 1 instance hot at all times, you will eliminate the
cold instance case entirely, but this comes at a cost:

* 1 CPU, 512 MiB RAM, 1 instance min, 1 instance max, running 7 days a week:
  * CPU: 2,628,000 vCPU-seconds: $42.98
  * RAM: 1,314,000 GiB-seconds: $1.73
  * Total for 1 month: $44.71
  * With 1 or 3 year-committed use (17%) discount: $37.11
* At this point, with the committed-use discount,
  you're only just barely ahead of a Compute Engine instance:
  * t2d-standard-1 - 1 vCPU, 4 GiB RAM - $0.04758/hour
  * Base cost for 1 month: $34.73
  * Debian/other free OS for 1 month: $0.00
  * Public IPv4 address for 1 month: $3.65
  * Total for 1 month: $38.38

### Variables

There are several variables you can use to increase the performance
of the application in the cloud:

* `min_instance_count` - The minimum number of instances to keep hot at any given time.
  * If you increase this, your costs will go up significantly (see above).
  * According to [Cloud Run documentation](https://cloud.google.com/run/docs/configuring/cpu-allocation#pricing-impact),
    the Recommender will monitor Cloud Run usage and recommend switching to
    always-allocated (`min_instance_count` 1) when it becomes cheaper.
    * How exactly this recommendation is delivered is unclear,
      but Google will apparently look out for your wallet's best interest.
* `max_instance_count` - The maximum number of instances that can be running
  at any given time, regardless of load.
  * Cloud Run will spin up more service instances when CPU utilization hits >60%
    on all existing instances until the existing instance count reaches this value.
* `discontinue_idle_instances` - Whether idle instances can be shut down or not.
  * If this is false, `min_instance_count` must be at least 1.
* `instance_memory_allocation` - How much memory to allocate for each instance.
  * If `discontinue_idle_instances` is false, this MUST be at least 512 MiB,
    per [Cloud Run documentation](https://cloud.google.com/run/docs/configuring/cpu-allocation#setting).

Because Cloud Run instances are billed per vCPU- and GiB-second,
including startup, shutdown, and idle time,
by default, you will only be billed for actual service usage.

## Closing Considerations

At what point should you switch from per-request to always-allocated?
This is something you would have to determine on an individual basis,
but you should keep this in mind:

* Unless you have a steadily increasing user base,
  you are unlikely to see the steady usage for which always-allocated Cloud Run instances make sense,
  because the vast majority of traffic will be cache hits.
* There will be some noticeable spikes if, for instance,
  your TRCH instance gets linked on a website like reddit,
  but most traffic will just hit the homepage and never actually use the service,
  leading to a one-time spike in cache-hits that result in few Cloud Run charges.
* Even if you have a large userbase, CloudFront will take up the majority of the costs,
  as most traffic will be recurring traffic (set-and-forget) from ad blockers polling for updates,
  which, due to the 30-day default caching policy,
  will result in mostly browser-cache hits,
  with few CloudFront hits (where it will usually be filled by the CloudFront cache),
  and even less traffic actually going to the service.
* In addition, as the user base grows,
  you will likely see a large number of users who share a subset of filters
  (i.e. who block the same channels), further increasing the cache hit rate.

Some applications, like [Compact YouTube](https://gitlab.com/lucadou/compact-youtube),
are completely static and can reside entirely within a CDN cache.
This application is basically the next-best thing -
stable filters for a website that rarely makes significant alterations
to its CSS structure.
