# TRCH - Twitch Recommended Channel Hider

Do you enjoy Twitch's channel recommendations but for the presence of a
select few streamers?
Look no further: now you can hide individual Twitch channels from your
recommendations using built-in functionality of uBlock/uBlock Origin
(and likely other adblockers as well).

![A before and after screenshot showing the removal of the Twitch channel "LinusTech" from the "Recommended Channels" section of the sidebar](docs/img/01-before-after-1.png)

### Table of Contents

* [Getting Started](#getting-started)
  * [Setup for End Users](#setup-for-end-users)
    * [Requirements](#requirements)
    * [Installation](#installation)
      * [uBlock Integration](#ublock-integration-recommended)
      * [Manual Setup](#manual-setup-advanced-users)
  * [Setup for Developers](#setup-for-developers)
* [Notes/FAQ](#notesfaq)
* [Versioning](#versioning)
* [Contributing](#contributing)
  * [Localization](#localization)
* [Authors and Acknowledgments](#authors-and-acknowledgments)
* [License](#license)

## Getting Started

### Setup for End Users

#### Requirements

* A modern browser with the [uBlock Origin](https://ublockorigin.com/)
  extension installed.

#### Installation

1. Go to the [Filter List Generator](https://trch.apps.lucadou.sh/).
2. Fill out the form according to your preferences.
3. Click the "Generate Filter List" button.
4. Pick your method:
   1. [uBlock Integration](#ublock-integration-recommended) (recommended method).
   2. [Manual](#manual-setup-advanced-users) (for advanced users).

##### uBlock Integration (Recommended)

1. Right-click on the link provided.
2. Go to the "uBlock" or "uBlock Origin" sub-menu.
3. Inside the sub-menu, click on "Subscribe to filter list...".
4. A new tab showing the contents of the filter list will open;
   switch to it if your browser does not automatically do so.
5. Click on the "Subscribe" button in the top right corner of the
   uBlock Asset Viewer tab.

##### Manual Setup (Advanced Users)

1. Click on the "Manual (Advanced)" option.
2. Copy the generated link to your clipboard.
3. Open your uBlock Dashboard.
4. Go to the "Filter lists" tab.
5. Scroll down to the bottom.
6. Check the box next to "Import...".
7. In the box that appears, paste the link you previously copied.
8. Press the "Apply Changes" button at the top of the page.
9. Press the "Update Now" button at the top of the page.

*(Manual method instructions were based on
https://github.com/gorhill/uBlock/wiki/Filter-lists-from-around-the-web)*

### Setup for Developers

To learn more about contributing, including development environment setup,
system requirements, testing, code style guidelines, and deployment,
see the [CONTRIBUTING](CONTRIBUTING.md#environment-setup) file for details.

## Notes/FAQ

> I want to hide *all* Recommended Channels, how do I accomplish this?

This application is intended only for hiding specific channels.
If you want to hide all Recommended Channels, you should do so with FrankerFaceZ:

1. Open the FrankerFaceZ Control Center.
2. Go to Appearance > Layout, then scroll down to "Display Recommended/Popular Channels"
   (or just search for the setting in the search bar).
3. Set "Display Recommended/Popular Channels" to "Never".

![The FrankerFaceZ Control Center, opened to the "Display Recommended/Popular Channels" setting, with the cursor hovering over the "Never" option](docs/img/03-hide-all-recommended-channels.png)

If you use 7TV or BTTV, just install FrankerFaceZ.
FrankerFaceZ will also fetch 7TV and BTTV emotes while delivering far more
functionality with better performance.

If you do not use FrankerFaceZ, install it: https://www.frankerfacez.com/

If you are using something besides a web browser to access Twitch,
you're out of luck.

> It no longer works, how do I fix it?

1. [Purge your uBlock caches](https://github.com/gorhill/uBlock/wiki/Dashboard:-Filter-lists#purge-all-caches)
2. [Update your uBlock filters](https://github.com/gorhill/uBlock/wiki/Dashboard:-Filter-lists#update-now)

> I did the above steps and it still isn't working

Chances are, Twitch has changed things up.
Feel free to open an [Issue](https://gitlab.com/lucadou/trch/-/issues)
to let us know they've broken things or
[contribute a patch](CONTRIBUTING.md),

> The filters do not work with <non-uBlock/uBlock Origin adblocker>.

Feel free to [contribute a patch](CONTRIBUTING.md),
but the maintainers do not use any other adblockers,
so we cannot guarantee it will remain usable.

> Should I use this if I or someone I share my computer with uses a
> screen reader?

Probably not.
We do not know how uBlock Origin affects the functionality of screen readers and
other accessibility software, but we can only assume it would negatively impact
them.

That said, if you use different user accounts or browsers/browser profiles,
you're fine.

> Why was this made?

Because we got tired of seeing certain streamers recommended in the
Twitch sidebar.

Even if you block a user on Twitch, they will still show up in the sidebar,
so the only way to prevent this is to automatically remove them when they
appear.

You could disable the Recommended Channels section entirely using FrankerFaceZ,
but not specific channels, so if you generally enjoy the recommendations,
this is the only option.

In order to make this available more broadly, we created a web application
to automate the process and to enable automatic updating in case Twitch
changes the structure of the sidebar.

> The website seems quite similar to that of the Compact YouTube project;
> was it based on that website?

The initial website template was forked off of
[Compact YouTube](https://gitlab.com/lucadou/compact-youtube),
and the server backend was forked off of
[SPFS - Simple Python File Server](https://gitlab.com/lucadou/spfs).

## Versioning

This project uses Semantic Versioning, aka
[SemVer](https://semver.org/spec/v2.0.0.html).

## Contributing

To learn more about contributing, including development environment setup
and code style guidelines, see the [CONTRIBUTING](CONTRIBUTING.md)
file for details.

### Localization

This application is currently available in the following languages:

* English

If you would like to make this application available in your langauge, feel
free to submit a merge request with translations.

To create translations, please see the [Localization section](CONTRIBUTING.md#localization)
of [CONTRIBUTING](CONTRIBUTING.md).

## Authors and Acknowledgments

* Luna Lucadou - Wrote the application in her free time.

## License

This project is licensed under the latest version of the GNU General Public License
(currently v3.0) - see the [LICENSE.md](LICENSE.md) file for details.
