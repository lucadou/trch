/**
 * This file is part of Twitch Recommended Channel Hider (TRCH),
 * a filter list generator for uBlock.
 * Copyright (C) 2024 Luna Lucadou
 *
 * TRCH is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * TRCH is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Compact YouTube.
 * If not, see <https://www.gnu.org/licenses/>.
 */
// LibreJS license indicator
// https://www.gnu.org/software/librejs/manual/html_node/Setting-Your-JavaScript-Free.html#License-tags
// @license magnet:?xt=urn:btih:1f739d935676111cfff4b4693e3816e664797050&dn=gpl-3.0.txt GPL-v3-or-Later

'use strict';

/**
 * @typedef {Object} FormContents
 * @property {String} ChannelName - the Twitch channel to hide
 */

/**
 * Reads the contents of the form and returns them to you as a FormContents Object
 *
 * @returns {FormContents} - the contents of the form
 */
function getFormSelections() {
    return {
        'ChannelName': document.forms.filterForm.ChannelName.value,
    };
}

/**
 * When the form is submitted, reads in all its contents and generates a link
 * to the user's requested configuration.
 * Then, it inserts the link into a modal which it makes visible to the user.
 *
 * @param {Event} event - the event that triggered this function call
 * @return {undefined} - nothing
 */
function processForm(event) {
    // Get form selections
    const formSelections = getFormSelections();

    /* Generate links for user
     * "/lists/" is added to ensure window.location.href not containing an
     * ending '/' does not cause problems, and then we call replaceAll to
     * de-duplicate '/'s
     */
    let filterURL = window.location.protocol + '//' +
        (window.location.host + window.location.pathname + '/lists/' + formSelections.ChannelName)
            .replaceAll('//', '/');
    let easyAddURL = 'https://subscribe.adblockplus.org/?location=' + filterURL +
        '&title=Twitch%20Recommended%20Channel%20Hider%20%28Channel%3A%20' + formSelections.ChannelName+ '%29';

    // Post links on page
    document.getElementById('filterURLEasyAdd').innerHTML =
        '<a href="' + easyAddURL + '">' + easyAddURL + '</a>';
    document.getElementById('filterURLAdvanced').innerHTML =
        '<a href="' + filterURL + '">' + filterURL + '</a>';
}

/**
 * When the form is submitted, determine if the form has been populated
 * correctly and proceeds to open the modal.
 * If not, shows the invalid fields in the form.
 *
 * @param {Event} event - the event that triggered this function call
 * @param {Element} form - the configuration form being submitted
 * @param {bootstrap.Modal} filterModal - the modal to show (if form is valid)
 * @return {undefined} - nothing
 */
function handleFormSubmit(event, form, filterModal) {
    event.preventDefault();

    if (form.checkValidity()) {
        processForm(event);
        filterModal.show();
    } else {
        form.reportValidity();
    }
}

window.addEventListener('load', function () {
    // Enable tooltips
    // https://getbootstrap.com/docs/5.2/components/tooltips/#enable-tooltips
    const tooltipTriggerList = document.querySelectorAll('[data-bs-toggle="tooltip"]');
    const tooltipList = [...tooltipTriggerList].map(tooltipTriggerEl => new bootstrap.Tooltip(tooltipTriggerEl));

    const form = document.getElementById('filterForm');
    const formBtn = document.getElementById('formSubmit');
    const filterModal = new bootstrap.Modal(document.getElementById('filterURLModal'));
    form.addEventListener('submit', function(event) {
        handleFormSubmit(event, form, filterModal);
    })
    formBtn.addEventListener('click', function(event) {
        handleFormSubmit(event, form, filterModal);
    });

    console.log('Loaded JS');
});
// @license-end
