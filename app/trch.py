#!/bin/python3
"""
This file is part of Twitch Recommended Channel Hider (TRCH),
a filter list generator for uBlock.
Copyright (C) 2024 Luna Lucadou

TRCH is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

TRCH is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TRCH.
If not, see <https://www.gnu.org/licenses/>.
"""
import datetime
import logging
import os
import secrets

import flask
from flask import (
    Flask,
    flash,
    make_response,
    redirect,
    render_template,
    request,
)
from flask_babel import Babel, gettext


def get_locale() -> str:
    """
    Configures Babel translations for a given request.

    https://phrase.com/blog/posts/python-localization-flask-applications/

    :return: str
    """
    return request.accept_languages.best_match(app.config["LANGUAGES"].keys())


app: flask.app.Flask = Flask(__name__)
app.config["LANGUAGES"] = {"en": "English"}
app.secret_key = secrets.token_bytes(32)
# We have to have a secret key for flashes, but since we do not persist any
# data, it can change at any time with no side effects.
babel: Babel = Babel(app)
babel.init_app(app, locale_selector=get_locale)
cache_days: int = int(os.getenv("CACHE_EXPIRATION_DAYS", "30"))
cache_period_seconds: int = cache_days * 24 * 60 * 60


@app.route("/", methods=["GET"])
def home() -> flask.Response:
    """
    The homepage of the application. Provides the channel name prompt.

    :return: the homepage
    """
    # https://flask.palletsprojects.com/en/2.1.x/api/#flask.make_response
    # https://flask.palletsprojects.com/en/2.1.x/api/#flask.render_template
    resp: flask.Response = make_response(
        render_template(
            "index.html",
        )
    )

    return resp


@app.route("/lists", methods=["GET"])
@app.route("/lists/", methods=["GET"])
def filter_lists() -> flask.Response:
    """
    No channel was provided to the filter list generator.

    :return: a redirect to the homepage
    """
    logging.warning(gettext("log.info.client.channel.missing"))
    flash(gettext("flash.error.client.channel.missing"))
    return redirect("/", code=301)


@app.route("/lists/<channel>", methods=["GET"])
def filter_list(channel: str) -> flask.Response:
    """
    Builds the filter list for a given Twitch channel.

    :param channel: the Twitch channel to hide
    :return: the filter list
    """
    resp: flask.Response = make_response(
        render_template(
            "lists/list.txt",
            channel=channel,
            channel_lowercase=channel.lower(),
            timestamp=str(datetime.datetime.now(tz=datetime.timezone.utc)),
            validity_period=str(cache_days),
        )
    )
    resp.mimetype = "text/plain"
    logging.info(gettext("log.info.server.list.gen-success %(name)s", name=channel))

    return resp


@app.after_request
def add_cache_headers(response: flask.Response) -> flask.Response:
    """
    Adds caching responses to all outbound requests
    (since this application is stateless).

    :param response: the outbound Response
    :return: the outbound Response, with cache headers added
    """
    response.cache_control.max_age = cache_period_seconds
    return response


if __name__ == "__main__":
    app.run(debug=True, host="0.0.0.0", port=int(os.environ.get("PORT", 5000)))
