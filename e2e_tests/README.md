# E2E Tests

This directory contains end-to-end tests for TRCH.
They use two different test harnesses: pytest and unittest.

The reason for this is simple:

* In order to test the cache length adjustments, we have to restart TRCH.
* pytest does not make this easy, but it is trivial to do with unittest.
  * pytest does allow for restarting the server for each test via
    defining the server start/stop method as a `pytest.fixture()`,
    but this does so for *all* tests, leading to lots of time being wasted
    just waiting on TRCH to start.
  * unittest lets you easily achieve this granularity,
    but it's not as nice to work with.
* If pytest has made this use case simple, feel free to submit an MR
  discontinuing the usage of unittests entirely.
