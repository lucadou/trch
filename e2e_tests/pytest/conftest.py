import os
import random
import re
import subprocess
import time

import pytest
import requests


# Inspired by:
# https://stackoverflow.com/a/42156088/15264046
# https://stackoverflow.com/a/35394239/15264046
class Helpers:

    @staticmethod
    def get_cache_header_max_age(resp: requests.Response) -> int:
        cache_regex: str = r"max-age=(\d+)"
        max_age: str = re.match(cache_regex, resp.headers.get("Cache-Control"))[1]
        return int(max_age)


@pytest.fixture()
def helpers():
    return Helpers


def pytest_sessionstart(session):
    # Setup
    # Store starting directory
    pytest.starting_dir = os.getcwd()
    # Determine webserver port
    port: int = random.randint(10000, 20000)
    # Navigate to app directory
    if "e2e_tests" in os.getcwd():
        os.chdir("../..")
    # Set cache age environment variable
    os.environ["CACHE_EXPIRATION_DAYS"] = "1"
    # Spawn server
    pytest.flask_process = subprocess.Popen(
        ["flask", "--app", "app/trch", "run", "--port", str(port)]
    )
    # Give the server some time to come up
    time.sleep(2.0)
    pytest.base_url = f"http://localhost.lucadou.sh:{str(port)}/"

    # Test if server is live
    _ = requests.get(pytest.base_url)
    # If no exceptions were thrown, it is listening
    print("Testing can now begin")


def pytest_sessionfinish(session, exitstatus):
    # Teardown
    # Kill server
    pytest.flask_process.kill()
    # Return to previous directory
    os.chdir(pytest.starting_dir)
