import datetime
import re

import pytest
import requests


def test_get_home(helpers):
    resp = requests.get(pytest.base_url)
    assert resp.status_code == 200
    assert helpers.get_cache_header_max_age(resp) > 1
    assert "TRCH - Twitch Recommended Channel Hider" in resp.text
    assert "You may also enjoy" in resp.text


def test_copyright_year_updated(helpers):
    resp = requests.get(pytest.base_url)
    current_year: int = datetime.datetime.now().year
    copyright_year_regex: str = r"&copy; (?:\d+-)?" + str(current_year)
    assert type(re.match(copyright_year_regex, resp.text)) is not None


def test_get_lists_flashes_error(helpers):
    resp = requests.get(pytest.base_url + "/lists")
    assert "No channel specified" in resp.text
    assert resp.url == pytest.base_url
    assert resp.status_code == 200
    assert resp.history[0].status_code == 301


def test_get_list(helpers):
    resp = requests.get(pytest.base_url + "/lists/somechannel")
    assert resp.status_code == 200
    assert "channel=somechannel" in resp.text
