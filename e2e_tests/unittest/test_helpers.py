import os
import random
import subprocess
import time

import requests


class ServerLifecycleManager:
    def __init__(self):
        self.starting_dir = os.getcwd()
        self.port: int = random.randint(10000, 20000)
        self.flask_process: subprocess.Popen[str] | None = None
        self.base_url: str = ""

    def start_server(self):
        # Navigate to app directory
        if "e2e_tests" in os.getcwd():
            os.chdir("../..")
        self.flask_process = subprocess.Popen(
            ["flask", "--app", "app/trch", "run", "--port", str(self.port)]
        )
        # Give the server some time to come up
        time.sleep(2.0)
        self.base_url = f"http://localhost.lucadou.sh:{str(self.port)}/"

        # Test if server is live
        _ = requests.get(self.base_url)
        # If no exceptions were thrown, it is listening
        print("Testing can now begin")

    def stop_server(self):
        if self.flask_process:
            self.flask_process.kill()

        os.chdir(self.starting_dir)
