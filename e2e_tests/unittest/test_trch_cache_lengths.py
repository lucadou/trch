import os
import unittest

import requests

from e2e_tests.unittest.test_helpers import ServerLifecycleManager


class E2ETestShortCacheTrch(unittest.TestCase):

    base_url = None
    server_manager = None
    expected_cache_header = "max-age=86400"

    @classmethod
    def setUpClass(cls) -> None:
        os.environ["CACHE_EXPIRATION_DAYS"] = "1"
        cls.server_manager = ServerLifecycleManager()
        cls.server_manager.start_server()
        cls.base_url = cls.server_manager.base_url

    @classmethod
    def tearDownClass(cls) -> None:
        cls.server_manager.stop_server()

    def test_home_cache_length_set_1_day(self):
        resp: requests.Response = requests.get(self.base_url)
        self.assertEqual(resp.headers["Cache-Control"], self.expected_cache_header)

    def test_lists_cache_length_set_1_days(self):
        resp: requests.Response = requests.get(self.base_url + "/lists/")
        self.assertEqual(resp.headers["Cache-Control"], self.expected_cache_header)

    def test_list_cache_length_set_1_day(self):
        resp: requests.Response = requests.get(self.base_url + "/lists/somechannel")
        self.assertEqual(resp.headers["Cache-Control"], self.expected_cache_header)


class E2ETestLongCacheTrch(unittest.TestCase):

    base_url = None
    server_manager = None
    expected_cache_header = "max-age=2592000"

    @classmethod
    def setUpClass(cls) -> None:
        os.environ["CACHE_EXPIRATION_DAYS"] = "30"
        cls.server_manager = ServerLifecycleManager()
        cls.server_manager.start_server()
        cls.base_url = cls.server_manager.base_url

    @classmethod
    def tearDownClass(cls) -> None:
        cls.server_manager.stop_server()

    def test_home_cache_length_set_30_days(self):
        resp: requests.Response = requests.get(self.base_url)
        self.assertEqual(resp.headers["Cache-Control"], self.expected_cache_header)

    def test_lists_cache_length_set_30_days(self):
        resp: requests.Response = requests.get(self.base_url + "/lists/")
        self.assertEqual(resp.headers["Cache-Control"], self.expected_cache_header)

    def test_list_cache_length_set_30_days(self):
        resp: requests.Response = requests.get(self.base_url + "/lists/somechannel")
        self.assertEqual(resp.headers["Cache-Control"], self.expected_cache_header)


if __name__ == "__main__":
    unittest.main()
