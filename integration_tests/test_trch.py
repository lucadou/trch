import os

import pytest
import werkzeug

from app import trch


@pytest.fixture()
def app():
    app = trch.app
    yield app


@pytest.fixture()
def client(app):
    return app.test_client()


@pytest.fixture()
def runner(app):
    return app.test_cli_runner()


def test_cache_headers(client):
    resp: werkzeug.test.TestResponse = client.get("/")
    assert resp.cache_control.max_age > 1


def test_get_home(client):
    resp: werkzeug.test.TestResponse = client.get("/")
    assert resp.status_code == 200
    assert "TRCH - Twitch Recommended Channel Hider" in resp.data.decode()


def test_get_lists(client):
    resp: werkzeug.test.TestResponse = client.get("/lists")
    assert resp.status_code == 301
    assert resp.location == "/"

    resp: werkzeug.test.TestResponse = client.get("/lists/")
    assert resp.status_code == 301
    assert resp.location == "/"


def test_get_list_lowercase_channel(client):
    resp: werkzeug.test.TestResponse = client.get("/lists/foo")
    assert resp.status_code == 200
    assert (
        "! Title: Twitch Recommended Channel Hider (Channel: foo)" in resp.data.decode()
    )
    assert (
        'www.twitch.tv##div.side-nav-section > .tw-transition-group > div:has(a[href="/foo"])'
        in resp.data.decode()
    )
    assert "! Homepage: https://gitlab.com/lucadou/trch" in resp.data.decode()


def test_get_list_mixed_case_channel(client):
    resp: werkzeug.test.TestResponse = client.get("/lists/fOoBaR")
    assert resp.status_code == 200
    assert (
        "! Title: Twitch Recommended Channel Hider (Channel: fOoBaR)"
        in resp.data.decode()
    )
    assert (
        'www.twitch.tv##div.side-nav-section > .tw-transition-group > div:has(a[href="/foobar"])'
        in resp.data.decode()
    )


def test_get_list_uppercase_channel(client):
    resp: werkzeug.test.TestResponse = client.get("/lists/BAR")
    assert resp.status_code == 200
    assert (
        "! Title: Twitch Recommended Channel Hider (Channel: BAR)" in resp.data.decode()
    )
    assert (
        'www.twitch.tv##div.side-nav-section > .tw-transition-group > div:has(a[href="/bar"])'
        in resp.data.decode()
    )
