#!/bin/sh
set -e

echo "Installing GCloud CLI..."
curl -fsSL https://packages.cloud.google.com/apt/doc/apt-key.gpg | gpg --dearmor -o /etc/apt/keyrings/cloud.google.gpg
echo "deb [signed-by=/etc/apt/keyrings/cloud.google.gpg] https://packages.cloud.google.com/apt cloud-sdk main" | tee -a /etc/apt/sources.list.d/google-cloud-sdk.list
apt-get update -y && apt-get install google-cloud-cli google-cloud-sdk -y

echo "Installing AWS CLI..."
curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"
unzip awscliv2.zip
./aws/install
rm -rf aws/
rm -rf awscliv2.zip

echo "Decoding key files..."
base64 -d "${AWS_TF_SA_KEY}" > "${AWS_KEY_TARGET}"
base64 -d "${GCP_TF_SA_KEY}" > "${GCP_KEY_TARGET}"

echo "Authenticating to AWS and GCP..."
/usr/local/bin/aws configure import --csv "file://${AWS_KEY_TARGET}"
aws configure import --csv "file://${AWS_KEY_TARGET}"
gcloud auth activate-service-account "${GCP_TF_SA}" --key-file="${GCP_KEY_TARGET}"

# Isolate AWS profile name for environment variable
# (result is discarded; this is just here for an
#  explanation of what the command does in gitlab-ci.yml):
AWS_PROFILE="$(sed 's/,/ /' "${AWS_KEY_TARGET}" | awk 'NR==2 {print $1}')"
export AWS_PROFILE
# Explanation:
#   sed 's/,/ /' ${AWS_KEY_TARGET}  - Splits CSV into a space-separated sheet
#   awk 'NR==2 {print $1}'  - Isolate the first column of the second row
# This isolates the profile name, which is required for the AWS_PROFILE variable.
# Without this variable set (and "aws configure import" does not set it for you),
# you will get the following error:
#   Error: No valid credential sources found
#     with provider["registry.opentofu.org/hashicorp/aws"],
#     on provider.tf line 7, in provider "aws":
#      7: provider "aws" {
