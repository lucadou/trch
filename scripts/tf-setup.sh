#!/bin/sh
set -e

# Fetch Tofu backend.conf
echo "Decoding TF backend.conf..."
TF_CONF_TARGET="tofu/backend.conf"
base64 -d "${TF_CONF_FILE}" > "${TF_CONF_TARGET}"

# Check for Tofu in apt
echo "Is tofu available in the current Debian runner?"
apt search tofu
# TODO if this ever returns anything, use apt tofu instead of manual install tofu

# Install Tofu manually in the interim
echo "If tofu was listed, delete the following manual steps in favor of apt install:"
curl --proto '=https' --tlsv1.2 -fsSL https://get.opentofu.org/install-opentofu.sh -o install-opentofu.sh
chmod +x install-opentofu.sh
./install-opentofu.sh --install-method deb
rm install-opentofu.sh

# Init Tofu directory
cd tofu/
tofu init -backend-config=backend.conf
cd -
