# https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/acm_certificate_validation
resource "aws_acm_certificate" "cdn_cert" {
  domain_name       = var.domain
  validation_method = "DNS"

  options {
    certificate_transparency_logging_preference = "ENABLED"
  }
}

resource "aws_acm_certificate_validation" "cdn_cert_validation" {
  certificate_arn         = aws_acm_certificate.cdn_cert.arn
  validation_record_fqdns = [for record in aws_route53_record.cert_validation : record.fqdn]
}

resource "aws_cloudfront_distribution" "cdn" {
  aliases = [var.domain]
  comment = "TRCH Service CDN"
  enabled = true
  origin {
    # We have to cut out the "https://" from the Cloud Run URI or else we get
    # one of the least helpful error messages I have ever seen:
    #   Error: creating CloudFront Distribution: InvalidArgument: The parameter origin name cannot contain a colon.
    # Yes, it is mixing up the domain and origin. I have no idea why.
    # I spent the better part of an hour debugging this before realizing what was going on.
    domain_name = substr(google_cloud_run_v2_service.trch_service.uri, 8, 28)
    origin_id   = var.aws_cloudformation_origin_name

    custom_origin_config {
      http_port              = 80
      https_port             = 443
      origin_protocol_policy = "https-only"
      origin_ssl_protocols   = ["TLSv1.2"]
    }

    origin_shield {
      enabled              = var.aws_cloudfront_origin_shield_enabled
      origin_shield_region = var.aws_region
    }
  }

  # DRO is required or you will get error 403 "Your client does not have permission to get URL / from this server."
  default_root_object = "/"
  http_version    = "http2and3"
  is_ipv6_enabled = true
  price_class     = var.aws_cloudfront_price_class

  default_cache_behavior {
    # By default, everything should be cached, which is fine since this application is basically stateless.
    allowed_methods        = ["GET", "HEAD"]
    cached_methods         = ["GET", "HEAD"]
    compress               = true
    viewer_protocol_policy = "redirect-to-https"
    target_origin_id       = var.aws_cloudformation_origin_name

    # Have to mess with TTLs to set object caching to "Use origin cache headers" without a separate cache policy
    # https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cloudfront_distribution#cache-behavior-arguments
    # https://github.com/hashicorp/terraform-provider-aws/issues/19382
    default_ttl = 86400
    min_ttl = 0
    max_ttl = 31536000

    forwarded_values {
      headers      = ["Accept-Language"]
      query_string = true
      cookies {
        forward = "none"
      }
    }
  }

  restrictions {
    geo_restriction {
      restriction_type = "none"
    }
  }

  viewer_certificate {
    acm_certificate_arn      = aws_acm_certificate.cdn_cert.arn
    minimum_protocol_version = "TLSv1.2_2021"
    ssl_support_method       = "sni-only"
    # Terraform documentation does a really poor job explaining this attribute, so here goes:
    # - We have to supply ssl_support_method because we specify acm_certificate_arn.
    # - It has 3 options:
    #  - "static-ip" presumably refers to the "Legacy clients support" option
    #    when you select a custom SSL cert in the CloudFront GUI,
    #    which allocates static IPs for the distribution at each edge location (for $600/mo extra).
    #    This would have to be for *really* legacy clients, as SNI was introduced in RFC 3546 (in June 2003).
    #  - "sni-only" means no static IP, only https://en.wikipedia.org/wiki/Server_Name_Indication?useskin=vector
    #    (TL;DR - HTTP has supported "virtual hosts" - different websites on the same host & port -
    #     since HTTP/1.1 in 1997, and the browser specifies which domain a request is for via the "Host" header;
    #     this is the same principle, but applied to HTTPS).
    #    This is the default setting when you create a distribution via the CloudFront GUI.
    #  - "vip" means...I have no idea what.
    #    https://docs.aws.amazon.com/AmazonCloudFront/latest/DeveloperGuide/cnames-https-dedicated-ip-or-sni.html
    #    AWS' documentation doesn't even mention "vip", so I am truly at a loss as to what this refers to.
  }
}
