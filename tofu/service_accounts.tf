resource "google_service_account" "trch_runner" {
  account_id = "trcf-runner"
  display_name = "TRCH Runner Service Account"
  description = "Service account the Cloud Run service will execute under"
}

resource "google_service_account" "cloud_run_invoker" {
  account_id = "trcf-invoker"
  display_name = "TRCH Invoker Service Account"
  description = "Service account for Cloud Run invocation"
}
