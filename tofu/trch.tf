# NOTE: After deployment, you MUST manually go into the GCP console > Cloud Run > this Cloud Run service > Security
# and set "Authentication" to "Allow unauthenticated invocations",
# as Terraform does not provide a way to do this automatically.
# TODO figure out how to authenticate from CloudFront so we do not need to worry about this manual step (#1)
resource "google_cloud_run_v2_service" "trch_service" {
  name     = "trch"
  location = var.gcp_region
  ingress  = "INGRESS_TRAFFIC_ALL"

  template {
    containers {
      image = "${var.gcp_region}-docker.pkg.dev/${var.gcp_project}/${var.gcp_ar_repo_name}/${var.gcp_ar_image_name}:latest"

      resources {
        cpu_idle = var.discontinue_idle_instances
        limits   = {
          cpu    = 1
          memory = var.instance_memory_allocation
        }
      }

      env {
        name  = "CACHE_EXPIRATION_DAYS"
        value = var.cache_length
      }
    }
    scaling {
      min_instance_count = var.min_instance_count
      max_instance_count = var.max_instance_count
    }
    service_account = google_service_account.trch_runner.email
    timeout         = "60s"
  }

  traffic {
    percent = 100
    type    = "TRAFFIC_TARGET_ALLOCATION_TYPE_LATEST"
  }
}

# TODO: Figure out how to get CloudFront to authenticate using this service account (#1)
resource "google_cloud_run_v2_service_iam_member" "tcrh_invoker" {
  name     = google_cloud_run_v2_service.trch_service.name
  location = google_cloud_run_v2_service.trch_service.location
  member   = "serviceAccount:${google_service_account.cloud_run_invoker.email}"
  role     = "roles/run.invoker"
}
