variable "gcp_project" {
  description = "The GCP project to deploy into"
  type = string
}

# Maintainer note: If you modify this, make sure to also modify your tfvars.sh file and the readme
# since the region variable is also used for Cloud Build.
variable "gcp_region" {
  description = "The GCP region to deploy into"
  type = string
  default = "us-east4"
}

variable "gcp_zone" {
  description = "The GCP zone to deploy into"
  type = string
  default = "us-east4-c"
}

# Maintainer note: If you modify this, make sure to also modify your tfvars.sh file and the readme
# since the image name variable is also used for Cloud Build.
variable "gcp_ar_image_name" {
  description = "The name of the built service container in Google Cloud Artifact Registry"
  type = string
  default = "trch-service"
}

# Maintainer note: If you modify this, make sure to also modify your tfvars.sh file and the readme
# since the image name variable is also used for Cloud Build.
variable "gcp_ar_repo_name" {
  description = "The name of the Google Cloud Artifact Registry repository that Cloud Build will store the container in"
  type = string
  default = "trch-docker-images"
}

variable "aws_region" {
  description = "The AWS region to deploy into"
  type = string
  default = "us-east-1"
}

variable "aws_route53_zone_id" {
  description = "The Route 53 zone ID to create DNS records in"
  type = string
}

# For more info on CloudFront price costs, see:
# - CONTRIBUTING.md
# - https://aws.amazon.com/cloudfront/pricing/
# - https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cloudfront_distribution#price_class
variable "aws_cloudfront_price_class" {
  description = "The price class to use for the CloudFront distribution"
  type = string
  default = "PriceClass_100"
}

# This may save money if your TRCH instance has heavy traffic
# For more info on CloudFront price costs, see:
# - CONTRIBUTING.md
# - https://aws.amazon.com/cloudfront/pricing/ (Additional Features tab)
variable "aws_cloudfront_origin_shield_enabled" {
  description = "Whether or not to use the Origin Shield caching layer for the CloudFront distribution"
  type = bool
  default = true
}

variable aws_cloudformation_origin_name {
  description = "The name for the CloudFormation origin that points to the Cloud Run endpoint"
  type = string
  default = "trch-service-origin"
}

variable "cache_length" {
  description = "The amount of time responses can be cached for, in days"
  type = number
  default = 30
}

variable "domain" {
  description = "The domain the application will be accessible at through the CDN"
  type = string
}

# Performance-tuning variables
# See PERFORMANCE.md for more information
variable "min_instance_count" {
  description = "The minimum number of service instances to keep running at any given time"
  type = number
  default = 0
}

variable "max_instance_count" {
  description = "The maximum number of concurrent service instances that may be running at any given time"
  type = number
  default = 2
}

variable "discontinue_idle_instances" {
  description = "Whether idle service instances should be shuttered after a period of inactivity (must be false if min_instance_count >= 1)"
  type = bool
  default = true
}

variable "instance_memory_allocation" {
  description = "How much memory each service instance will use (must be >=512Mi if discontinue_idle_instances is false)"
  type = string
  default = "256Mi"
}
